package controller

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/chukmunnlee/learn-gingonic/entity"
	"gitlab.com/chukmunnlee/learn-gingonic/myvalidator"
	"gitlab.com/chukmunnlee/learn-gingonic/service"
)

type VideoController interface {
	GetAll() []entity.Video
	Save(entity.Video) (*entity.Video, error)
	FindById(string) (*entity.Video, error)
}

type VideoControllerImpl struct {
	Service           service.VideoService
	DirectorValidator *validator.Validate
}

func New(svc service.VideoService) VideoController {

	v := validator.New()
	v.RegisterValidation("is-director", myvalidator.ValidateDirector)

	return &VideoControllerImpl{Service: svc, DirectorValidator: v}
}

func (ctrl *VideoControllerImpl) GetAll() []entity.Video {
	return ctrl.Service.GetAll()
}

func (ctrl *VideoControllerImpl) Save(vid entity.Video) (*entity.Video, error) {
	if err := ctrl.DirectorValidator.Struct(vid); nil != err {
		return nil, err
	}
	retVal := ctrl.Service.Save(vid)
	return &retVal, nil
}

func (ctrl *VideoControllerImpl) FindById(id string) (*entity.Video, error) {
	return ctrl.Service.FindById(id)
}
