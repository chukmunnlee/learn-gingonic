package service

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/chukmunnlee/learn-gingonic/entity"
)

type VideoService interface {
	GetAll() []entity.Video
	Save(entity.Video) entity.Video
	FindById(string) (*entity.Video, error)
}

type VideoServiceImpl struct {
	Videos []entity.Video
}

func New() VideoService {
	return &VideoServiceImpl{Videos: make([]entity.Video, 0)}
}

func (impl *VideoServiceImpl) GetAll() []entity.Video {
	return impl.Videos
}

func (impl *VideoServiceImpl) Save(vid entity.Video) entity.Video {
	newVid := entity.Video(vid)
	newVid.Id = uuid.New().String()[0:8]
	impl.Videos = append(impl.Videos, newVid)
	return newVid
}

func (impl *VideoServiceImpl) FindById(id string) (*entity.Video, error) {
	for _, v := range impl.Videos {
		if id == v.Id {
			return &v, nil
		}
	}

	return nil, fmt.Errorf("Cannot find Video id: %s", id)
}
