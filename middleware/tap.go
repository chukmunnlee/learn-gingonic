package middleware

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func Tap() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		log.Printf("*** TAP: %s\n", time.Now().Format(time.RFC1123Z))
		ctx.Next()
	}
}
