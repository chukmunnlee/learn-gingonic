module gitlab.com/chukmunnlee/learn-gingonic

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/go-playground/validator/v10 v10.4.2 // indirect
	github.com/google/uuid v1.2.0 // indirect
)
