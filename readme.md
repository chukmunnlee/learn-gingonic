# Learning Gin Gonic

[Gin HTTP Framework](https://www.youtube.com/playlist?list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w)

- [Basics](https://www.youtube.com/watch?v=qR0WnWL2o1Q&list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w)
- [Middleware](https://www.youtube.com/watch?v=Ypwv1mFZ5vU&list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w)
- [Data binding and Validation](https://www.youtube.com/watch?v=lZsbPtGfGIs&list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w)
- [HTML and Templates](https://www.youtube.com/watch?v=sDJLQMZzzM4&list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w)
