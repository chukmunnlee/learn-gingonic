package entity

type Video struct {
	Id          string `json:"id,omitempty" xml:"id,omitempty" form:"id,omitempty"`
	Title       string `json:"title" xml:"title" form:"title" binding:"min=2,max=32"`
	Description string `json:"description" xml:"description" form:"description" binding:"max=128"`
	URL         string `json:"url,omitempty" xml:"url,omitempty" form:"url,omitempty" binding:"required,url"`
	//Director    Person `json:"director" binding:"required"`
}

type Person struct {
	FirstName string `json:"firstName" binding:"required" validate:"is-director"`
	LastName  string `json:"lastName" binding:"required" validate:"is-director"`
	Age       uint8  `json:"age" binding:"gte=15,lte=100"`
	Email     string `json:"email" validate:"required,email"`
}
