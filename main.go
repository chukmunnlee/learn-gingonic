package main

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/chukmunnlee/learn-gingonic/controller"
	"gitlab.com/chukmunnlee/learn-gingonic/entity"
	"gitlab.com/chukmunnlee/learn-gingonic/middleware"
	"gitlab.com/chukmunnlee/learn-gingonic/service"
)

func main() {

	videoCtrl := controller.New(service.New())

	router := gin.Default()

	// Serve static files
	router.Static("/css", "./templates/css")
	// Serve HTML template
	router.LoadHTMLGlob("./templates/*.html")

	//router := gin.New()
	// Added with gin.Default()
	//router.Use(gin.Recovery(), gin.Logger())

	// Enable cors
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{"*"}
	router.Use(cors.New(corsConfig))

	// Add tap
	router.Use(middleware.Tap())

	// Group REST endpoints under /api
	apiRoot := router.Group("/api")
	{
		apiRoot.GET("/videos", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, gin.H{
				"data": videoCtrl.GetAll(),
			})
		})

		apiRoot.GET("/video/:id", func(ctx *gin.Context) {
			id := ctx.Param("id")
			if vid, err := videoCtrl.FindById(id); nil != err {
				ctx.JSON(http.StatusNotFound, gin.H{
					"error": err.Error(),
				})
			} else {
				ctx.JSON(http.StatusOK, gin.H{
					"data": *vid,
				})
			}
		})

		apiRoot.POST("/video", middleware.BasicAuth("fred", "fredfred"),
			func(ctx *gin.Context) {
				vid := entity.Video{}
				//ctx.BindJSON(&vid)
				if err := ctx.ShouldBindJSON(&vid); nil != err {
					ctx.JSON(http.StatusBadRequest, gin.H{
						"error": err.Error(),
					})
					return
				}

				if retVal, err := videoCtrl.Save(vid); nil != err {
					ctx.JSON(http.StatusBadRequest, gin.H{
						"error": err.Error(),
					})
					return
				} else {
					ctx.JSON(http.StatusOK, gin.H{
						"id":   retVal.Id,
						"data": *retVal,
					})
				}
			})
	}

	// view routes
	viewRoot := router.Group("/views")
	{
		viewRoot.GET("/videos", func(ctx *gin.Context) {
			videos := videoCtrl.GetAll()
			data := gin.H{
				"title":  "List of all Videos",
				"videos": videos,
			}
			ctx.HTML(http.StatusOK, "index.html", data)
		})
	}

	router.Run(":3000")
}
